**BANK ACCOUNT KATA**

 

**User Stories:**

**US 1:**

In order to save money

As a bank client

I want to make a deposit in my account
we should provide the account id where we are going to withdraw money and the amount we want to deposit
Endpoint: /api/v1/operations/deposit/{idAccount}/{amount}


*US 2:*

In order to retrieve some or all of my savings

As a bank client

I want to make a withdrawal from my account

Case 1 : partial retrieve : we should provide the account id where we are going to withdraw money and the amount we want to retrieve

Endpoint: /api/v1/operations/withdraw/{idAccount}/{amount}

Case 2 : total retrieve : we should provide the account

Endpoint: /api/v1/operations/withdraw/{idAccount}

*US 3:*

In order to check my operations

As a bank client

I want to see the history (operation, date, amount, balance) of my operations

Endpoint: /api/v1/operations

 

**Swagger url :**

http://localhost:8080/swagger-ui.html

 


