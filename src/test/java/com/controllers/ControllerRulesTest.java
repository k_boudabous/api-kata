package com.controllers;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.core.importer.ImportOptions;
import com.tngtech.archunit.lang.syntax.elements.GivenClassesConjunction;
import org.junit.Before;
import org.junit.Test;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

public class ControllerRulesTest {

    protected GivenClassesConjunction controllers;
    protected JavaClasses javaClasses;

    @Before
    public void init() {
        this.controllers = classes().that().haveNameMatching(".*Controller");
        this.javaClasses = new ClassFileImporter().importClasspath(new ImportOptions()
                .with(new ImportOption.DoNotIncludeJars())
                .with(new ImportOption.DoNotIncludeTests())
                .with(new ImportOption.DoNotIncludeArchives()));
    }

    @Test
    public void controller_classes_should_be_suffixed_as_such() {
        classes()
                .that().resideInAPackage("..controllers..")
                .should().haveSimpleNameEndingWith("Controller")
                .check(javaClasses);
    }

    @Test
    public void controllers_should_reside_in_a_controller_package() {
        classes()
                .that().haveNameMatching("Controller")
                .should().resideInAPackage("..controllers..")
                .because("Controllers should reside in a package '..controllers..'")
                .check(javaClasses);
    }

    @Test
    public void controller_classes_should_have_spring_controller_or_rest_controller_annotations() {
        classes()
                .that().resideInAPackage("..controllers..")
                .should().beAnnotatedWith(RestController.class).orShould().beAnnotatedWith(Controller.class)
                .check(javaClasses);
    }

}
