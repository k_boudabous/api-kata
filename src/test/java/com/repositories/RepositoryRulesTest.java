package com.repositories;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.core.importer.ImportOptions;
import com.tngtech.archunit.lang.syntax.elements.GivenClassesConjunction;
import org.junit.Before;
import org.junit.Test;
import org.springframework.stereotype.Repository;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

public class RepositoryRulesTest {

    protected GivenClassesConjunction repositories;
    protected JavaClasses javaClasses;

    @Before
    public void init() {
        this.repositories = classes().that().haveNameMatching(".*Repository");
        this.javaClasses = new ClassFileImporter().importClasspath(new ImportOptions()
                .with(new ImportOption.DoNotIncludeJars())
                .with(new ImportOption.DoNotIncludeTests())
                .with(new ImportOption.DoNotIncludeArchives()));
    }

    @Test
    public void repository_classes_should_be_suffixed_as_such() {
        classes()
                .that().resideInAPackage("..repositories..")
                .should().haveSimpleNameEndingWith("Repository")
                .check(javaClasses);
    }

    @Test
    public void repositories_should_reside_in_a_repository_package() {
        classes()
                .that().haveNameMatching("Repository")
                .should().resideInAPackage("..repositories..")
                .because("Repositories should reside in a package '..repositories..'")
                .check(javaClasses);
    }

    @Test
    public void repository_classes_should_have_spring_repository_annotation() {
        classes()
                .that().resideInAPackage("..repositories..")
                .and().haveNameMatching("Repository")
                .should().beAnnotatedWith(Repository.class)
                .check(javaClasses);
    }

    @Test
    public void repositories_should_only_be_accessed_by_services() {
        classes()
                .that().resideInAPackage("..repositories..")
                .should().onlyBeAccessed().byAnyPackage("..services..")
                .check(javaClasses);
    }

}
