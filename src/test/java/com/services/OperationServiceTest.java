package com.services;

import com.constants.OperationType;
import com.dto.OperationDto;
import com.entities.Account;
import com.entities.Operation;
import com.entities.User;
import com.repositories.AccountRepository;
import com.repositories.OperationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.constants.ErrorConstant.ACCOUNT_IS_NOT_FOUND;
import static com.constants.ErrorConstant.INSUFICIENT_BALANCE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OperationServiceTest {

    @Mock
    OperationRepository operationRepository;

    @Mock
    AccountRepository accountRepository;

    @InjectMocks
    OperationService operationService;

    @Test
    public void deposit_amount_should_return_operation_with_new_balance() {
        Account account = new Account(Long.valueOf(1), LocalDate.now(), 100.f, new User(), Long.valueOf(1));
        when(accountRepository.findById(Long.valueOf(1))).thenReturn(java.util.Optional.of(account));
        Account newAccount = new Account(Long.valueOf(1), LocalDate.now(), 200.f, new User(), Long.valueOf(1));
        when(accountRepository.save(any())).thenReturn(newAccount);
        Operation operation = new Operation(Long.valueOf(1), LocalDate.now(), OperationType.DEPOSIT, "", 200.f, newAccount);
        when(operationRepository.save(any())).thenReturn(operation);
        OperationDto result = operationService.depositMoney(Long.valueOf("1"), 100.f);
        assertNotNull(result);
        assertEquals(result.getAmount(), 100.f, 0);
        assertNotNull(result.getAccount());
        assertEquals(result.getAccount().getBalance(), 200.f, 0);
    }

    @Test
    public void deposit_amount_with_not_existing_acount_should_throw_exception() {
        when(accountRepository.findById(Long.valueOf(1))).thenThrow(new RuntimeException(ACCOUNT_IS_NOT_FOUND));
        try {
            OperationDto result = operationService.depositMoney(Long.valueOf("1"), 100.f);
        } catch (RuntimeException e) {
            assertEquals(e.getMessage(), ACCOUNT_IS_NOT_FOUND);

        }
    }

    @Test
    public void withdraw_money_should_return_operation_with_new_balance() {

        Account account = new Account(Long.valueOf(1), LocalDate.now(), 100.f, new User(), Long.valueOf(1));
        when(accountRepository.findById(Long.valueOf(1))).thenReturn(java.util.Optional.of(account));
        Account newAccount = new Account(Long.valueOf(1), LocalDate.now(), 50.f, new User(), Long.valueOf(1));
        when(accountRepository.save(any())).thenReturn(newAccount);
        Operation operation = new Operation(Long.valueOf(1), LocalDate.now(), OperationType.WITHDRAWAL, "", 200.f, newAccount);
        when(operationRepository.save(any())).thenReturn(operation);
        OperationDto result = operationService.withdrawMoney(Long.valueOf("1"), 50.f);
        assertNotNull(result);
        assertEquals(result.getAmount(), 50.f, 0);
        assertNotNull(result.getAccount());
        assertEquals(result.getAccount().getBalance(), 50.f, 0);

    }

    @Test
    public void withdraw_amount_with_not_existing_acount_should_throw_exception() {
        when(accountRepository.findById(Long.valueOf(1))).thenThrow(new RuntimeException(ACCOUNT_IS_NOT_FOUND));
        try {
            OperationDto result = operationService.withdrawMoney(Long.valueOf("1"), 100.f);
        } catch (RuntimeException e) {
            assertEquals(e.getMessage(), ACCOUNT_IS_NOT_FOUND);

        }
    }

    @Test
    public void withdraw_amount_with_not_suficient_balance_should_throw_exception() {
        Account account = new Account(Long.valueOf(1), LocalDate.now(), 100.f, new User(), Long.valueOf(1));
        when(accountRepository.findById(Long.valueOf(1))).thenReturn(java.util.Optional.of(account));
        try {
            OperationDto result = operationService.withdrawMoney(Long.valueOf("1"), 1000.f);
        } catch (RuntimeException e) {
            assertEquals(e.getMessage(), INSUFICIENT_BALANCE);

        }
    }

    @Test
    public void withdraw_all_money_should_return_operation_with_zero_balance() {

        Account account = new Account(Long.valueOf(1), LocalDate.now(), 100.f, new User(), Long.valueOf(1));
        when(accountRepository.findById(Long.valueOf(1))).thenReturn(java.util.Optional.of(account));
        Account newAccount = new Account(Long.valueOf(1), LocalDate.now(), 0.f, new User(), Long.valueOf(1));
        when(accountRepository.save(any())).thenReturn(newAccount);
        Operation operation = new Operation(Long.valueOf(1), LocalDate.now(), OperationType.WITHDRAWAL, "", 100.f, newAccount);
        when(operationRepository.save(any())).thenReturn(operation);
        OperationDto result = operationService.withdrawAll(Long.valueOf("1"));
        assertNotNull(result);
        assertEquals(result.getAmount(), 100.f, 0);
        assertNotNull(result.getAccount());
        assertEquals(result.getAccount().getBalance(), 0.f, 0);

    }

    @Test
    public void withdraw_all_amount_with_not_existing_acount_should_throw_exception() {
        when(accountRepository.findById(Long.valueOf(1))).thenThrow(new RuntimeException(ACCOUNT_IS_NOT_FOUND));
        try {
            OperationDto result = operationService.withdrawAll(Long.valueOf("1"));
        } catch (RuntimeException e) {
            assertEquals(e.getMessage(), ACCOUNT_IS_NOT_FOUND);

        }
    }

    @Test
    public void find_all_should_return_operations() {
        Account account = new Account(Long.valueOf(1), LocalDate.now(), 100.f, new User(), Long.valueOf(1));
        List<Operation> opeations = new ArrayList<>();
        Operation operation = new Operation(Long.valueOf(1), LocalDate.now(), OperationType.WITHDRAWAL, "", 200.f, account);
        opeations.add(operation);
        opeations.add(operation);
        opeations.add(operation);
        when(operationRepository.findAll()).thenReturn(opeations);
        List<OperationDto> result = operationService.findAll();
        assertEquals(result.size(), 3);
    }
}
