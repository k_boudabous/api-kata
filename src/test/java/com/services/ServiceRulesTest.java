package com.services;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.core.importer.ImportOptions;
import com.tngtech.archunit.lang.syntax.elements.GivenClassesConjunction;
import org.junit.Before;
import org.junit.Test;
import org.springframework.stereotype.Service;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

public class ServiceRulesTest  {

    protected GivenClassesConjunction services;
    protected JavaClasses javaClasses;

    @Before
    public void init() {
        this.services = classes().that().haveNameMatching(".*Service");
        this.javaClasses = new ClassFileImporter().importClasspath(new ImportOptions()
                .with(new ImportOption.DoNotIncludeJars())
                .with(new ImportOption.DoNotIncludeTests())
                .with(new ImportOption.DoNotIncludeArchives()));
    }

    @Test
    public void service_classes_should_be_suffixed_as_such() {
        classes()
                .that().resideInAPackage("..services..")
                .should().haveSimpleNameEndingWith("Service")
                .check(javaClasses);
    }

    @Test
    public void services_should_reside_in_a_service_package() {
        classes()
                .that().haveNameMatching("Service")
                .should().resideInAPackage("..services..")
                .because("Services should reside in a package '..services..'")
                .check(javaClasses);
    }

    @Test
    public void services_should_only_be_accessed_by_controllers() {
        classes()
                .that().resideInAPackage("..service..")
                .should().onlyBeAccessed().byAnyPackage("..controllers..", "..services..")
                .check(javaClasses);
    }

    @Test
    public void service_classes_should_have_spring_service_annotation() {
        classes()
                .that().resideInAPackage("..services..")
                .and().haveNameMatching("Service")
                .should().beAnnotatedWith(Service.class)
                .check(javaClasses);
    }
}
