package com.entities;


import com.constants.OperationType;
import com.dto.OperationDto;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "OPERATION",schema="public")

public class Operation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long operationId;

    private LocalDate operationDate;

    private OperationType operationType;

    private String description;

    private float amount;

    @ManyToOne(fetch = FetchType.LAZY, optional = false,cascade = CascadeType.PERSIST)
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    public OperationDto toDto() {
        return OperationDto.builder()
                .operationDate(operationDate)
                .description(description)
                .amount(amount)
                .account(account.toDto())
                .build();
    }
}
