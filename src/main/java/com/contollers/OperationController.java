package com.contollers;


import com.dto.OperationDto;
import com.services.OperationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(OperationController.PATH)
public class OperationController {

    private static final Logger logger = LogManager.getLogger(OperationController.class);

    public static final String PATH = "/api/v1/operations";
    @Autowired
    private OperationService operationService;

    @Autowired
    public void setUserService(OperationService operationService) {
        this.operationService = operationService;
    }

    @GetMapping
    @ApiOperation("get all operations")
    public List<OperationDto> findAll() {
        logger.info("get all operations");
        return this.operationService.findAll();
    }

    @PostMapping("/deposit/{id}/{amount}")
    @ResponseBody
    @ApiOperation("deposit money")
    public OperationDto depositMoney(@PathVariable long id, @ApiParam("amount to add") @PathVariable Long amount){
        logger.info("deposit money");
        return operationService.depositMoney(id,amount);
    }

    @PostMapping("/withdraw/{id}/{amount}")
    @ResponseBody
    @ApiOperation("withdraw part of money")
    public OperationDto withdrawMoney(@PathVariable long id, @ApiParam("amount to remove") @PathVariable Long amount){
        logger.info("withdraw part of money");
        return operationService.withdrawMoney(id,amount);
    }

    @PostMapping("/withdrawAll/{id}")
    @ResponseBody
    @ApiOperation("withdraw All money")
    public OperationDto withdrawAll(@PathVariable long id){
        logger.info("withdraw all money");
        return operationService.withdrawAll(id);
    }
}
