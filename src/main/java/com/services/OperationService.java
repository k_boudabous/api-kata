package com.services;

import com.constants.ErrorConstant;
import com.constants.OperationType;
import com.dto.OperationDto;
import com.entities.Account;
import com.entities.Operation;
import com.repositories.AccountRepository;
import com.repositories.OperationRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static com.constants.ErrorConstant.*;
import static com.constants.ErrorConstant.ACCOUNT_IS_NOT_FOUND;

@Service
public class OperationService {

    private static final Logger logger = LogManager.getLogger(OperationService.class);


    private OperationRepository operationRepository;
    private AccountRepository accountRepository;

    @Autowired
    public void setOperationrRepository(OperationRepository operationRepository) {
        this.operationRepository = operationRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    public List<OperationDto> findAll() {
        List<Operation> operationList = operationRepository.findAll();
        return operationList.stream().map(e -> e.toDto()).collect(Collectors.toList());
    }

    public OperationDto depositMoney(Long accountId, float amount) {
        Operation operation = new Operation();
        Account account = accountRepository.findById(accountId)
                .orElseThrow(() -> new RuntimeException(ACCOUNT_IS_NOT_FOUND)); // we should create notFoundException
        float newBalance = account.getBalance() + amount;
        account = updateAccountWithNewBalance(account, newBalance);
        operation = updateAndsaveOperation(OperationType.DEPOSIT, operation, account);
        operation.setAmount(amount);
        return operation.toDto();
    }

    public OperationDto withdrawMoney(Long accountId, float amount) {
        Operation operation = new Operation();
        Account account = accountRepository.findById(accountId)
                .orElseThrow(() -> new RuntimeException(ACCOUNT_IS_NOT_FOUND));
        if (account.getBalance() >= amount) {
            float newBalance = account.getBalance() - amount;
            account = updateAccountWithNewBalance(account, newBalance);
            operation = updateAndsaveOperation(OperationType.WITHDRAWAL, operation, account);
            operation.setAmount(amount);
            return operation.toDto();
        } else {
            throw new RuntimeException(INSUFICIENT_BALANCE);
        }
    }


    public OperationDto withdrawAll(Long accountId) {
        Operation operation = new Operation();
        Account account = accountRepository.findById(accountId)
                .orElseThrow(() -> new RuntimeException(ACCOUNT_IS_NOT_FOUND));
        float amount = account.getBalance();
        account = updateAccountWithNewBalance(account, 0);
        operation = updateAndsaveOperation(OperationType.WITHDRAWAL, operation, account);
        operation.setAmount(amount);
        return operation.toDto();
    }

    private Operation updateAndsaveOperation(OperationType operationType, Operation operation, Account account) {
        operation.setAccount(account);
        operation.setOperationType(operationType);
        operation.setOperationDate(LocalDate.now());
        operationRepository.save(operation);
        logger.info("balance updated.");
        return operation;
    }


    private Account updateAccountWithNewBalance(Account account, float newAmount) {
        account.setBalance(newAmount);
        return accountRepository.save(account);
    }

}
