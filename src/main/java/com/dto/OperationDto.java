package com.dto;

import com.constants.OperationType;
import com.entities.Operation;
import lombok.*;

import java.time.LocalDate;

@Builder
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class OperationDto {

    private LocalDate operationDate;
    private OperationType operationType;
    private String description;
    private float amount;
    private AccountDto account;

    public Operation toEntity() {
        return Operation.builder()
                .operationDate(operationDate)
                .description(description)
                .amount(amount)
                .account(account.toEntity())
                .build();
    }
}
