package com.dto;

import com.entities.Account;
import lombok.*;

import java.time.LocalDate;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AccountDto {

    private LocalDate creationDate;
    private float balance;
    private UserDto user;
    private Long userId;

    public Account toEntity() {
        return Account.builder()
                .creationDate(creationDate)
                .balance(balance)
                .user(user.toEntity())
                .build();
    }
}
