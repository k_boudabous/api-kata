package com.constants;

public class ErrorConstant {

    private ErrorConstant() {
    }

    public static final String ACCOUNT_IS_NOT_FOUND = "account is not found";
    public static final String INSUFICIENT_BALANCE = "insufficient balance";
}
