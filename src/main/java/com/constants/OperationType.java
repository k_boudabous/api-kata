package com.constants;

public enum OperationType {

    WITHDRAWAL("retrait"),
    DEPOSIT("depot");

    private final String type;

    OperationType(final String type) {
        this.type = type;
    }
}
